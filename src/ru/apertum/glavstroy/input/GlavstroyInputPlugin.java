/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.glavstroy.input;

import java.awt.Frame;
import ru.apertum.qsystem.client.model.QButton;
import ru.apertum.qsystem.common.QLog;
import ru.apertum.qsystem.common.cmd.CmdParams;
import ru.apertum.qsystem.common.cmd.JsonRPC20;
import ru.apertum.qsystem.common.cmd.RpcGetAllServices;
import ru.apertum.qsystem.common.model.IClientNetProperty;
import ru.apertum.qsystem.common.model.INetProperty;
import ru.apertum.qsystem.common.model.QCustomer;
import ru.apertum.qsystem.extra.IWelcome;
import ru.apertum.qsystem.server.ServerProps;
import ru.apertum.qsystem.server.model.QAdvanceCustomer;
import ru.apertum.qsystem.server.model.QProperty;
import ru.apertum.qsystem.server.model.QService;
import ru.apertum.qsystem.server.model.QServiceTree;

/**
 *
 * @author Egorov-EV
 */
public class GlavstroyInputPlugin implements IWelcome {

    final static private String SERVICE = "GetServiceForInputData";
    final static private String SECTION = "glavstroy-phone-checker";
    final static private String URL = "url";
    final static private String USR = "user";
    final static private String PWD = "password";

    @Override
    public void start(IClientNetProperty icnp, RpcGetAllServices.ServicesForWelcome sfw) {
    }

    @Override
    public boolean showPreInfoDialog(QButton qb, Frame frame, INetProperty inp, String string, String string1, boolean bln, boolean bln1, int i) {
        return true;
    }

    @Override
    public String showInputDialog(QButton qb, Frame frame, boolean bln, INetProperty inp, boolean bln1, int i, String string, QService qs) {
        return null; // возвращаем null, т.е. пусть работает дефолтный диалог
    }

    final private QProperty url = ServerProps.getInstance().getProperty(SECTION, URL);
    final private QProperty usr = ServerProps.getInstance().getProperty(SECTION, USR);
    final private QProperty pwd = ServerProps.getInstance().getProperty(SECTION, PWD);
    final private CmdParams params = new CmdParams();
    final private JsonRPC20 cmd = new JsonRPC20(SERVICE, params);

    @Override
    public StandInParameters handleStandInParams(QButton qb, StandInParameters sip) {
        final QService service = QServiceTree.getInstance().getById(sip.serviceId);
        if (!service.getInput_required()) {
            return sip;
        }
        
        if (url == null || usr == null || pwd == null) {
            QLog.l().logger().error("No parameters. Must be present parameters:\n\"glavstroy-phone-checker.url\"=" + url + "\n\"glavstroy-phone-checker.user\"=" + usr + "\n\"glavstroy-phone-checker.password\"=" + pwd);
            return sip;
        }
        if (url.getValue() == null || usr.getValue() == null || pwd.getValue() == null) {
            QLog.l().logger().error("Bad parameters. \n url=" + url.getValue() + "\n user=" + usr.getValue() + "\n password=" + pwd.getValue());
            return sip;
        }
        params.textData = sip.inputData;
        try {
            final long serviceId = Network.sendRpc(url.getValue(), usr.getValue(), pwd.getValue(), cmd);
            if (QServiceTree.getInstance().hasById(serviceId) && serviceId > 0) { // только если услуга присутствует
                sip.serviceId = serviceId;
                QLog.l().logger().trace("Service for client was changed on id=" + serviceId + " which was obtained from 1C.");
            } else {
                QLog.l().logger().warn("Service was not found by id=" + serviceId + " which was obtained from 1C.");
            }
        } catch (Throwable ex) {
            QLog.l().logger().error("Responce from 1C was destroyed.", ex);
        }
        return sip;
    }

    @Override
    public void buttonPressed(QButton qb, QService qs) {
    }

    @Override
    public void readyNewCustomer(QButton qb, QCustomer qc, QService qs) {
    }

    @Override
    public void readyNewAdvCustomer(QButton qb, QAdvanceCustomer qac, QService qs) {
    }

    @Override
    public String getDescription() {
        return "Plugin for glavstroy checking phone number.";
    }

    @Override
    public long getUID() {
        return 456654L;
    }

}
