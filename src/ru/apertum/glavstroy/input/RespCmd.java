/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ru.apertum.glavstroy.input;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.LinkedList;
import ru.apertum.qsystem.common.cmd.JsonRPC20;

/**
 *
 * @author SBT-Egorov-EV
 */
public class RespCmd extends JsonRPC20 {

    @Expose
    @SerializedName("result")
    private Long result;

    public Long getResult() {
        return result;
    }

    public void setResult(Long result) {
        this.result = result;
    }

}
